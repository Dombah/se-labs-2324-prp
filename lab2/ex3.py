import json
with open('ex2-text.csv', 'r', encoding="utf-8") as f:
    read_data = f.read().split('\n')
    employees = []
    firstRow = True
    for line in read_data:
        if firstRow:
            firstRow = False
            continue
        line_d = line.split(',')
        dic = {'employee': line_d[0], 'title':line_d[1], 'age': line_d[2], 'office': line_d[3]}
        employees.append(dic)
        
with open('ex4-employees.json', 'w', encoding='utf-8') as f:
    json.dump(employees, f)