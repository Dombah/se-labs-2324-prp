
with open('ex2-text.csv', 'r', encoding="utf-8") as f:
    read_data = f.read().split('\n')
f.closed

with open('ex2-employees.txt', 'w', encoding='utf-8') as f: 
    for line in read_data:
        line_data = line.split(',')
        employee_Name = line_data[0]
        job = line_data[1]
        f.write(f'{employee_Name}, {job}\n')

with open('ex2-locations.txt', 'w', encoding='utf-8') as f: 
    for line in read_data:
        line_data = line.split(',')
        employee_Name = line_data[0]
        office = line_data[3]
        f.write(f'{employee_Name}, {office}\n')
